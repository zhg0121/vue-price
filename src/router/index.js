import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
	routes: [
    {
      path: "/home" , component: ()=> import("../components/Price.vue")
    },
    {
      path: "/note" , component : () => import( "../components/NotePage.vue"),
    },
    {
      path: "/chart", component : () => import("../components/ChartPage.vue"),
    },
    {
      path: "/", component : () => import("../components/Home.vue"),
    }
	],
});
