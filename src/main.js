import Vue from "vue";
import App from "./App.vue";
import router from './router';
import ElementUI from "element-ui";
import moment from 'moment';
import "element-ui/lib/theme-chalk/index.css";

Vue.config.productionTip = false;
Vue.use(ElementUI, {
  size:'small'
});
Vue.use(moment);
new Vue({
  router,
	render: (h) => h(App),
}).$mount("#app");

