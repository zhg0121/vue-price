import axios from 'axios';
import qs from 'qs';

const service = axios.create({
    baseURL: '//127.0.0.1:8000/api/',
    timeout: 5000,
});



service.interceptors.request.use(config=>{
    if(!config.data){
        config.data=[];
    }

    console.log(qs.stringify(config.data));

    return config;
},error=>{
    console.log(error);

    return Promise.reject(error);
});

service.interceptors.response.use(response=>{
    return response;
},error=>{
    console.log('error: '+ error);
    return Promise.reject(error);
});

export default service;