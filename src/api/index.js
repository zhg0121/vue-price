import request from './request';

export function login(data){
    return request({
        url:'login',
        method:'post',
        data: data,
    });
}

export function register(data){
    return request({
        url:'register',
        method:'post',
        data: data,
    });
}

export function logout(data){
    return request({
        url:'logout',
        method:'post',
        data: data,
    });
}

export function testSession(data){
    return request({
        url:'testSession',
        method:'post',
        data: data,
    });
}